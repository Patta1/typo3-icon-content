<?php
/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Lavitto\IconContent\Form\Element;

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Backend\Form\NodeFactory;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Class IconContentElement
 *
 * @package Lavitto\IconContent\Form\Element
 */
class IconContentElement extends AbstractFormElement
{

    /**
     * List of all icons by json-configuration
     *
     * @var array
     */
    protected $iconList = [];

    /**
     * Container objects give $nodeFactory down to other containers.
     *
     * @param NodeFactory $nodeFactory
     * @param array $data
     */
    public function __construct(NodeFactory $nodeFactory, array $data)
    {
        parent::__construct($nodeFactory, $data);
        $this->loadAssets();
        $this->initializeIconList();
    }

    /**
     * Renders the icon selector
     *
     * @return array
     */
    public function render(): array
    {
        $result = $this->initializeResultArray();
        $result['html'] = $this->getSelector();
        return $result;
    }

    /**
     * Loads the icon list from the json configuration file
     */
    protected function initializeIconList(): void
    {
        $iconList = ExtensionManagementUtility::extPath('icon_content', 'Configuration/Json/FontAwesomeIcons.json');
        $iconListContent = GeneralUtility::getUrl($iconList);
        $iconList = json_decode($iconListContent, true);
        foreach ($iconList as $icon) {
            $this->iconList[substr($icon, 0, 3)][] = $icon;
        }
    }

    /**
     * Loads the backend css and javascript
     */
    protected function loadAssets(): void
    {
        $extPath = ExtensionManagementUtility::extPath('icon_content', 'Resources/Public');
        $extRelPath = str_replace(Environment::getPublicPath() . '/', '../', $extPath);

        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile($extRelPath . '/Libraries/fontawesome-free-5.13.0/css/all.min.css');
        $pageRenderer->addCssFile($extRelPath . '/Css/Backend.min.css');
        $pageRenderer->loadRequireJsModule('TYPO3/CMS/IconContent/IconContentElement');
    }

    /**
     * Return the icon selector
     *
     * @return string
     */
    protected function getSelector(): string
    {
        $name = $this->data['parameterArray']['itemFormElName'];
        $id = $this->data['parameterArray']['itemFormElID'];
        $value = $this->data['parameterArray']['itemFormElValue'];

        $out = '<div class="form-control-clearable">';
        $out .= '<input type="text" name="icon-filter" value="" class="form-control" placeholder="' . LocalizationUtility::translate('LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:tt_content.iconcontent_iconcontent.icon.search') . '">';
        $out .= '<button type="button" class="close reset-icon-filter" tabindex="-1" aria-hidden="true">' . $this->getCloseIcon() . '</button>';
        $out .= '</div>';
        foreach ($this->iconList as $iconGroup => $icons) {
            $out .= '<div class="icon-list">';
            $out .= '<h5>' . LocalizationUtility::translate('LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:tt_content.iconcontent_iconcontent.icon.group.' . $iconGroup) . '</h5>';
            $out .= '<div class="row">';
            foreach ($icons as $icon) {
                $out .= $this->getIconSelector($icon, $icon === $value ? ' active' : '');
            }
            $out .= '<div class="empty hidden"><i>' . LocalizationUtility::translate('LLL:EXT:icon_content/Resources/Private/Language/Tca.xlf:tt_content.iconcontent_iconcontent.icon.search.empty') . '</i></div>';
            $out .= '</div></div>';
        }
        $out .= '<input type="hidden" name="' . $name . '" value="' . $value . '" id="' . $id . '" />';
        return '<div class="ext-icon-content">' . $out . '</div>';
    }

    /**
     * Returns the icon selector tag
     *
     * @param string $icon
     * @param string $active
     * @return string
     */
    protected function getIconSelector(string $icon, string $active = ''): string
    {
        $keywords = str_replace(
            ['fas ', 'far ', 'fab ', 'fa-', '-'],
            ['solid ', 'regular ', 'brand ', '', ' '],
            $icon
        );
        return '<div class="item' . $active . '" data-value="' . $icon . '" data-keywords="' . $keywords . '" title="' . $icon . '"><i class="' . $icon . ' fa-fw fa-2x"></i></div>';
    }

    /**
     * Returns the close icon by IconFactory
     *
     * @return string
     */
    protected function getCloseIcon(): string
    {
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        return $iconFactory->getIcon('actions-close', Icon::SIZE_SMALL)->render();
    }
}
