<?php
/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Lavitto\IconContent\ViewHelpers;

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class IconColorViewHelper
 *
 * @package Lavitto\IconContent\ViewHelpers
 */
class IconColorViewHelper extends AbstractViewHelper
{

    /**
     * Regex condition to check a hexadecimal color value
     */
    protected const HEX_COLOR_REGEX = '^#([0-9A-Fa-f]{3})|([0-9A-Fa-f]{6})$';

    /**
     * Initialize arguments.
     *
     * @return void
     */
    public function initializeArguments(): void
    {
        $this->registerArgument(
            'color',
            'string',
            'The color as hex-value (with #)',
            true
        );
        $this->registerArgument(
            'iconId',
            'int',
            'The id of the icon content element',
            true
        );
        $this->registerArgument(
            'link',
            'string',
            'The link of the icon',
            false
        );
    }

    /**
     * Adds the CSS to the page header
     */
    public function render(): void
    {
        $iconId = $this->arguments['iconId'] ?? 0;
        if ($iconId > 0) {
            $color = $this->isValidHexColor($this->arguments['color'] ?? '') === true ? $this->arguments['color'] : null;
            if ($color !== null) {
                $blockId = 'ce-icon-content-' . $iconId;
                $cssSelector = '#' . $blockId . ($this->arguments['link'] ? '>a' : '');
                $this->getPageRenderer()->addCssInlineBlock($blockId, $cssSelector . '{color:' . $color . '}');
            }
        }
    }

    /**
     * Checks an input if its a valid hexadecimal color
     *
     * @param string $color
     * @return bool
     */
    protected function isValidHexColor(string $color): bool
    {
        return preg_match('/' . self::HEX_COLOR_REGEX . '/', $color) === 1;
    }

    /**
     * Gets the PageRenderer
     *
     * @return PageRenderer
     */
    protected function getPageRenderer(): PageRenderer
    {
        return GeneralUtility::makeInstance(PageRenderer::class);
    }
}
