# TYPO3 Extension `Icon Content`

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg?style=for-the-badge)](https://paypal.me/pmlavitto)
[![Latest Stable Version](https://img.shields.io/packagist/v/lavitto/typo3-icon-content?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-icon-content)
[![TYPO3](https://img.shields.io/badge/TYPO3-icon_content-%23f49700?style=for-the-badge)](https://extensions.typo3.org/extension/icon_content/)
[![License](https://img.shields.io/packagist/l/lavitto/typo3-icon-content?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-icon-content)

> This extension adds an icon content element to your TYPO3 website. With the new content element you can add an icon of the Font Awesome Library instead of an image.

- **Demo**: [www.lavitto.ch/typo3-ext-icon-content](https://www.lavitto.ch/typo3-ext-icon-content)
- **Gitlab Repository**: [gitlab.com/lavitto/typo3-icon-content](https://gitlab.com/lavitto/typo3-icon-content)
- **TYPO3 Extension Repository**: [extensions.typo3.org/extension/icon_content](https://extensions.typo3.org/extension/icon_content/)
- **Found an issue?**: [gitlab.com/lavitto/typo3-icon-content/issues](https://gitlab.com/lavitto/typo3-icon-content/issues)

## 1. Introduction

### Features

- Based on extbase & fluid
- Simple and fast installation
- No configuration needed
- Easy to use icon selector with searchfield

## 2. Installation

### Installation using Composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org/). In your Composer based 
TYPO3 project root, just do `composer req lavitto/typo3-icon-content`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension `icon_content` with the extension manager module.

## 3. Minimal setup

1)  Include the static TypoScript of the extension.
3)  Include the static TypoScript (CSS) of the extension. **OPTIONAL**, if Bootstrap 4 is not available on your website. 
2)  Create an icon content element on a page

## 4. Administration

### Create content element

1) Create a new content element and select "Icon Content"
2) Add header/text and select the icon in the tab "Icon"
3) Define settings like the icon size, animation, color, rotation, position or border

## 5. Configuration

### Constants

This default properties can be changed by **Constant Editor**:

| Property           | Description                                    | Type      | Default value   |
| ------------------ | ---------------------------------------------- | --------- | --------------- |
| enableFontAwesome  | Includes Font Awesome CSS/Fonts to the website | boolean   | true            |

## 6. Contribute

Please create an issue at https://gitlab.com/lavitto/typo3-icon-content/issues.

**Please use GitLab only for bug-reports or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need private or personal support, contact us by email on [info@lavitto.ch](mailto:info@lavitto.ch). 

**Be aware that this support might not be free!**
