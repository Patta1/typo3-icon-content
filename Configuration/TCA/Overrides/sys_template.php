<?php
/**
 * This file is part of the "icon_content" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'icon_content',
    'Configuration/TypoScript',
    'Icon Content'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'icon_content',
    'Configuration/TypoScript/Css',
    'Icon Content CSS (optional, only recommended if Bootstrap 4 is not used)'
);
